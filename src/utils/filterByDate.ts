import { LiveByCountryDto, WorldWIPDto } from '../services/api/types';

export const filterByDate = <T extends WorldWIPDto | LiveByCountryDto>(
  data?: T[],
  start?: Date | null,
  end?: Date | null,
): T[] => {
  if (!data || !start) {
    return [];
  }

  return data.filter((i) => {
    const timeStamp = new Date(i.Date).getTime();
    const periodStart = start.getTime();
    if (end) {
      const periodEnd = end.getTime();
      return timeStamp > periodStart && timeStamp < periodEnd;
    }
    return timeStamp > periodStart;
  });
};
