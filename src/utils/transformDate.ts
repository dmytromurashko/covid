import { LiveByCountryDto, WorldWIPDto } from '../services/api/types';

export const transformDate = <T extends WorldWIPDto | LiveByCountryDto>(data?: T[]): T[] => {
  if (!data) {
    return [];
  }

  return data
    ?.sort((a, b) => new Date(a.Date).getTime() - new Date(b.Date).getTime())
    .map((i) => ({
      ...i,
      Date: i.Date.slice(0, 10).split('-').reverse().join('.'),
    }));
};
