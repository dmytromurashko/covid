import { GroupedLives, LiveByCountryDto } from '../../../services/api/types';

export const groupByDate = (array?: LiveByCountryDto[]) => {
  if (!array) {
    return [];
  }

  const groupedDates = array?.reduce((acc, val) => {
    acc[val.Date] = {
      Date: val.Date,
      Confirmed: String(
        acc[val.Date] ? Number(acc[val.Date].Confirmed) + Number(val.Confirmed) : val.Confirmed,
      ),
      Deaths: String(
        acc[val.Date] ? Number(acc[val.Date]?.Deaths) + Number(val.Deaths) : val.Deaths,
      ),
      Recovered: String(
        acc[val.Date] ? Number(acc[val.Date]?.Recovered) + Number(val.Recovered) : val.Recovered,
      ),
      Active: String(
        acc[val.Date] ? Number(acc[val.Date]?.Active) + Number(val.Active) : val.Active,
      ),
    };

    return acc;
  }, {} as Record<string, GroupedLives>);

  return Object.values(groupedDates);
};
