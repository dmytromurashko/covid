import {
  CartesianGrid,
  Legend,
  Bar,
  BarChart,
  Tooltip,
  XAxis,
  YAxis,
  ResponsiveContainer,
} from 'recharts';
import { LiveByCountryUIComponent } from './LiveByCountryUI.types';
import { Box, MenuItem, Select, TextField, Typography } from '@mui/material';
import DateAdapter from '@mui/lab/AdapterDateFns';
import DesktopDatePicker from '@mui/lab/DesktopDatePicker';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import { LiveByCountryFilters } from '../../../services/api/types';
import { SelectsWrapper } from '../../../components/SelectsWrapper/SelectsWrapper';

export const LiveByCountryUI: LiveByCountryUIComponent = ({
  data,
  start,
  country,
  countries,
  filter,
  handleStartChange,
  handleFilterChange,
  handleCountryChange,
}) => {
  return (
    <Box
      sx={{ display: 'flex' }}
      height="100vh"
      width="calc(100vw - 300px)"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      px={4}
    >
      <Typography variant="h4">Live by country and status after date</Typography>
      <Box mb={2} />
      <ResponsiveContainer width="99%" height={500}>
        <BarChart
          width={730}
          height={400}
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="Date" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey={filter} fill="#8884d8" />
        </BarChart>
      </ResponsiveContainer>
      <SelectsWrapper>
        <LocalizationProvider dateAdapter={DateAdapter}>
          <DesktopDatePicker
            label="Period start"
            inputFormat="MM/dd/yyyy"
            value={start}
            onChange={handleStartChange}
            renderInput={(params) => <TextField {...params} />}
          />
        </LocalizationProvider>
        <Select id="live-case-select" value={filter} label="Case" onChange={handleFilterChange}>
          {Object.values(LiveByCountryFilters).map((filter) => (
            <MenuItem value={filter} key={filter}>
              {filter}
            </MenuItem>
          ))}
        </Select>
        <Select id="country-select" value={country} label="Case" onChange={handleCountryChange}>
          {countries.map(({ Slug, Country }) => (
            <MenuItem value={Slug} key={Slug}>
              {Country}
            </MenuItem>
          ))}
        </Select>
      </SelectsWrapper>
    </Box>
  );
};
