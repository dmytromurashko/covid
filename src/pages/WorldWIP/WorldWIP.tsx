import { WorldWIPUI } from './WorldWIPUI/WorldWIPUI';
import { useGetWorldWIP } from './hooks/useGetWorldWIP/useGetWorldWIP';
import { WorldWIPDto } from '../../services/api/types';
import { ContentWrapper } from '../../components/ContentWrapper/ContentWrapper';
import { AxiosError } from 'axios';
import { useTitle } from '../../hooks/useTitle/useTitle';
import { transformDate } from 'utils/transformDate';
import { useContext } from 'react';
import { WorldWIPContext } from '../../contexts/WorldWIPContext/WorldWIPContext';
import { filterByDate } from '../../utils/filterByDate';

export default function WorldWIP() {
  useTitle('World WIP');

  const { filter, handleFilterChange, start, handleStartChange, end, handleEndChange } =
    useContext(WorldWIPContext);

  const { data, isLoading, error } = useGetWorldWIP(
    start?.toISOString() as string,
    end?.toISOString() as string,
  );

  return (
    <ContentWrapper isLoading={isLoading} error={error as AxiosError}>
      <WorldWIPUI
        data={transformDate(filterByDate(data, start, end)) as WorldWIPDto[]}
        start={start as Date}
        end={end as Date}
        filter={filter}
        handleStartChange={handleStartChange}
        handleEndChange={handleEndChange}
        handleFilterChange={handleFilterChange}
      />
    </ContentWrapper>
  );
}
