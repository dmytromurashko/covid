import { WorldWIPDto } from '../../../../services/api/types';
import { UseQueryResult } from 'react-query';

export type UseGetWorldWIP = (start: string, end: string) => UseQueryResult<WorldWIPDto[]>;
