import { useQuery } from 'react-query';
import { getWorldWIP } from '../../../../services/api/requests';
import { UseGetWorldWIP } from './useGetWorldWIP.types';

const QUERY_KEY = 'worldWIP';

export const useGetWorldWIP: UseGetWorldWIP = (start, end) =>
  useQuery([QUERY_KEY, start, end], async () => getWorldWIP(start, end), {
    retry: false,
    staleTime: 3600000,
  });
