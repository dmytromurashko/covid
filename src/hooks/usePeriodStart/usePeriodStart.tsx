import { useState } from 'react';

export const usePeriodStart = () => {
  const [start, setStart] = useState<Date | null>(new Date('01.01.2022'));

  const handleStartChange = (date: Date | null) => {
    setStart(date);
  };

  return {
    start,
    handleStartChange,
  };
};
