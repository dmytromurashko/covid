export type CountryDto = {
  Country: string;
  Slug: string;
  ISO2: string;
};

export type WorldWIPDto = {
  Date: string;
  NewConfirmed: number;
  NewDeaths: number;
  NewRecovered: number;
  TotalConfirmed: number;
  TotalDeaths: number;
  TotalRecovered: number;
};

export type LiveByCountryDto = {
  Country: string;
  CountryCode: string;
  Lat: string;
  Lon: string;
  Confirmed: string;
  Deaths: string;
  Recovered: string;
  Active: string;
  Date: string;
  LocationID: string;
};

export enum WorldWIPFilters {
  newConfirmed = 'NewConfirmed',
  newDeaths = 'NewDeaths',
  newRecovered = 'NewRecovered',
  totalConfirmed = 'TotalConfirmed',
  totalDeaths = 'TotalDeaths',
  totalRecovered = 'TotalRecovered',
}

export enum LiveByCountryFilters {
  confirmed = 'Confirmed',
  deaths = 'Deaths',
  recovered = 'Recovered',
  active = 'Active',
}

export enum LiveByCountryStatus {
  Confirmed = 'confirmed',
  Deaths = 'deaths',
  Recovered = 'recovered',
  Active = 'confirmed',
}

export type GroupedLives = {
  Confirmed: string;
  Deaths: string;
  Recovered: string;
  Active: string;
  Date: string;
};
