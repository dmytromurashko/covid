import { methods } from './methods';
import { COVID_DOMAIN } from './CONSTANTS';
import { CountryDto, LiveByCountryDto, WorldWIPDto } from './types';

const { get } = methods();

export async function getCountries(): Promise<CountryDto[]> {
  const { data } = await get(`${COVID_DOMAIN}/countries`);
  return data;
}

export async function getWorldWIP(start: string, end: string): Promise<WorldWIPDto[]> {
  const { data } = await get(`${COVID_DOMAIN}/world?from=${start}&to=${end}`);
  return data;
}

export async function getLiveByCountry(
  country: string,
  date_from: string,
  status: string,
): Promise<LiveByCountryDto[]> {
  const { data } = await get(
    `${COVID_DOMAIN}/live/country/${country}/status/${status}/date/${date_from}`,
  );
  return data;
}
