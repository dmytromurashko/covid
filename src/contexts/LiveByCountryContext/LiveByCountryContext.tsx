import React from 'react';
import { LiveByCountryCachedFilters } from './LiveByCountryContext.types';
import { LiveByCountryFilters } from '../../services/api/types';
import { usePeriodStart } from '../../hooks/usePeriodStart/usePeriodStart';
import { useCountryFilter } from '../../pages/LiveByCountry/hooks/useCountryFilter/useCountryFilter';
import { useCasesFilter } from '../../hooks/useCasesFilter/useCasesFilter';
import { useGetCountries } from '../../pages/LiveByCountry/hooks/useGetCountries/useGetCountries';

export const LiveByCountryContext = React.createContext<LiveByCountryCachedFilters>({
  start: new Date(),
  country: 'ukraine',
  countries: [],
  isFetchingCountries: true,
  countriesError: null,
  filter: LiveByCountryFilters.confirmed,
  handleStartChange: () => void 0,
  handleCountryChange: () => void 0,
  handleFilterChange: () => void 0,
});

export const LiveByCountryProvider: React.FC = ({ children }) => {
  const { start, handleStartChange } = usePeriodStart();
  const { country, handleCountryChange } = useCountryFilter();
  const { filter, handleFilterChange } = useCasesFilter<LiveByCountryFilters>(
    LiveByCountryFilters.confirmed,
  );

  const {
    data: countries,
    isLoading: isFetchingCountries,
    error: countriesError,
  } = useGetCountries();

  return (
    <LiveByCountryContext.Provider
      value={{
        start,
        country,
        filter,
        handleStartChange,
        handleCountryChange,
        handleFilterChange,
        countries,
        isFetchingCountries,
        countriesError,
      }}
    >
      {children}
    </LiveByCountryContext.Provider>
  );
};
