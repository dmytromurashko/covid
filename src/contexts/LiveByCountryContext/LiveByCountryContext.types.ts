import { CountryDto, LiveByCountryFilters } from '../../services/api/types';
import { SelectChangeEvent } from '@mui/material/Select';

export type LiveByCountryCachedFilters = {
  start: Date | null;
  country: string;
  filter: LiveByCountryFilters;
  countries?: CountryDto[];
  isFetchingCountries: boolean;
  countriesError: unknown;
  handleStartChange: (date: Date | null) => void;
  handleCountryChange: (event: SelectChangeEvent) => void;
  handleFilterChange: (event: SelectChangeEvent) => void;
};
