import { SelectChangeEvent } from '@mui/material/Select';
import { WorldWIPFilters } from '../../services/api/types';

export type WorldWIPCachedFilters = {
  start: Date | null;
  end: Date | null;
  filter: WorldWIPFilters;
  handleStartChange: (date: Date | null) => void;
  handleEndChange: (date: Date | null) => void;
  handleFilterChange: (event: SelectChangeEvent) => void;
};
