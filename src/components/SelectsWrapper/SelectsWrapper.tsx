import React from 'react';
import { StyledSelectsWrapper } from './SelectsWrapper.styles';

export const SelectsWrapper: React.FC = ({ children }) => {
  return (
    <StyledSelectsWrapper justifyContent="center" pt={2}>
      {children}
    </StyledSelectsWrapper>
  );
};
