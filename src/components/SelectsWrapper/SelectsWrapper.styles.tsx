import styled from 'styled-components';
import { Box } from '@mui/material';

export const StyledSelectsWrapper = styled(Box)`
  display: flex;
  flex-direction: row;

  > div {
    margin-right: 16px;
  }

  @media (max-width: 900px) {
    flex-direction: column;

    > div {
      margin-bottom: 16px;
    }
  }
`;
