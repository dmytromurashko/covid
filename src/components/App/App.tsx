import { BrowserRouter } from 'react-router-dom';
import RouterConfig from '../../navigation/RouterConfig';
import { QueryClient, QueryClientProvider } from 'react-query';
import { WorldWIPProvider } from '../../contexts/WorldWIPContext/WorldWIPContext';
import { LiveByCountryProvider } from '../../contexts/LiveByCountryContext/LiveByCountryContext';
import { AppWrapper } from './App.styles';

export default function App() {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        refetchOnWindowFocus: false,
      },
    },
  });

  return (
    <BrowserRouter>
      <QueryClientProvider client={queryClient}>
        <WorldWIPProvider>
          <LiveByCountryProvider>
            <AppWrapper>
              <RouterConfig />
            </AppWrapper>
          </LiveByCountryProvider>
        </WorldWIPProvider>
      </QueryClientProvider>
    </BrowserRouter>
  );
}
