import { Link } from 'react-router-dom';
import { Box, Divider, Drawer, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import { makeStyles } from '@mui/styles';
import { Home, Equalizer, AccountBalance } from '@mui/icons-material';

import { ABOUT, COUNTRY_STAT, WORLD_WIP } from '../../navigation/CONSTANTS';
import { SidebarComponent } from './Sidebar.types';
import { SidebarList } from './Sidebar.styles';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
  },
  drawerPaper: {
    width: '300px',
  },
}));

export const Sidebar: SidebarComponent = () => {
  const classes = useStyles();

  return (
    <Box flexBasis="300px">
      <Drawer
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <SidebarList sx={{ padding: 0 }}>
          <Link to={WORLD_WIP}>
            <ListItem button key="Global statistics">
              <Box sx={{ display: 'flex' }} justifyContent="spaceBetween">
                <ListItemIcon>
                  <Home />
                </ListItemIcon>
                <ListItemText primary="Global statistics" />
              </Box>
            </ListItem>
          </Link>

          <Divider />

          <Link to={COUNTRY_STAT}>
            <ListItem button key="Statistics by country">
              <Box sx={{ display: 'flex' }} justifyContent="spaceBetween">
                <ListItemIcon>
                  <Equalizer />
                </ListItemIcon>
                <ListItemText primary="Statistics by country" />
              </Box>
            </ListItem>
          </Link>

          <Divider />

          <Link to={ABOUT}>
            <ListItem button key="About">
              <Box sx={{ display: 'flex' }} justifyContent="spaceBetween">
                <ListItemIcon>
                  <AccountBalance />
                </ListItemIcon>
                <ListItemText primary="About" />
              </Box>
            </ListItem>
          </Link>

          <Divider />
        </SidebarList>
      </Drawer>
    </Box>
  );
};
