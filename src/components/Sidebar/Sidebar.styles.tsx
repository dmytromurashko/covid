import styled from 'styled-components';
import { Link, List } from '@mui/material';

export const SidebarLink = styled(Link)`
  display: flex;
  align-items: center;
`;

export const SidebarList = styled(List)`
  padding: 0;

  a {
    text-decoration: none;
    color: #000000;
  }
`;
